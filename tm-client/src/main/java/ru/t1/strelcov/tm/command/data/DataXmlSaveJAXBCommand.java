package ru.t1.strelcov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.request.DataXmlJAXBSaveRequest;
import ru.t1.strelcov.tm.enumerated.Role;

public class DataXmlSaveJAXBCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-xml-save-jaxb";
    }

    @Override
    @NotNull
    public String description() {
        return "Save entities data to xml file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML SAVE]");
        serviceLocator.getDataEndpoint().saveXmlJAXBData(new DataXmlJAXBSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
