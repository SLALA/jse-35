package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.TaskListResponse;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.marker.IntegrationCategory;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.service.PropertyService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final Integer port = propertyService.getServerPort();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @Nullable
    private String token;

    @Before
    public void before() {
        token = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
        taskEndpoint.clearTask(new TaskClearRequest(token));
        taskEndpoint.createTask(new TaskCreateRequest(token, "pr2", "pr2"));
        taskEndpoint.createTask(new TaskCreateRequest(token, "pr1", "pr1"));
    }

    @After
    public void after() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        authEndpoint.logout(new UserLogoutRequest());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void listTasksTest() {
        Assert.assertEquals(2, taskEndpoint.listTask(new TaskListRequest(token)).getList().size());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void listSortedTasksTest() {
        @NotNull final List<Task> list = taskEndpoint.listSortedTask(new TaskListSortedRequest(token, SortType.NAME.name())).getList();
        Assert.assertEquals(2, list.size());
        @NotNull final Comparator<Task> comparator = SortType.NAME.getComparator();
        Assert.assertEquals(list, list.stream().sorted(comparator).collect(Collectors.toList()));
    }

    @Category(IntegrationCategory.class)
    @Test
    public void createTaskTest() {
        int size = taskEndpoint.listTask(new TaskListRequest(token)).getList().size();
        taskEndpoint.createTask(new TaskCreateRequest(token, "pr2", "pr2"));
        Assert.assertEquals(size + 1, taskEndpoint.listTask(new TaskListRequest(token)).getList().size());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void clearTaskTest() {
        taskEndpoint.clearTask(new TaskClearRequest(token));
        TaskListResponse response = taskEndpoint.listTask(new TaskListRequest(token));
        Assert.assertEquals(0, taskEndpoint.listTask(new TaskListRequest(token)).getList().size());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void findByIdTaskTest() {
        @NotNull final Task expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        @NotNull final Task actualTask = taskEndpoint.findByIdTask(new TaskFindByIdRequest(token, expectedTask.getId())).getTask();
        Assert.assertEquals(expectedTask.getId(), actualTask.getId());
        Assert.assertEquals(expectedTask.getName(), actualTask.getName());
        Assert.assertEquals(expectedTask.getCreated(), actualTask.getCreated());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void findByNameTaskTest() {
        @NotNull final Task expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        @NotNull final Task actualTask = taskEndpoint.findByNameTask(new TaskFindByNameRequest(token, expectedTask.getName())).getTask();
        Assert.assertEquals(expectedTask.getId(), actualTask.getId());
        Assert.assertEquals(expectedTask.getName(), actualTask.getName());
        Assert.assertEquals(expectedTask.getCreated(), actualTask.getCreated());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void removeByIdTaskTest() {
        int size = taskEndpoint.listTask(new TaskListRequest(token)).getList().size();
        @NotNull final Task expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        taskEndpoint.removeByIdTask(new TaskRemoveByIdRequest(token, expectedTask.getId()));
        Assert.assertEquals(size - 1, taskEndpoint.listTask(new TaskListRequest(token)).getList().size());
        Assert.assertFalse(taskEndpoint.listTask(new TaskListRequest(token)).getList().contains(expectedTask));
    }

    @Category(IntegrationCategory.class)
    @Test
    public void removeByNameTaskTest() {
        int size = taskEndpoint.listTask(new TaskListRequest(token)).getList().size();
        @NotNull final Task expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        taskEndpoint.removeByNameTask(new TaskRemoveByNameRequest(token, expectedTask.getName()));
        Assert.assertEquals(size - 1, taskEndpoint.listTask(new TaskListRequest(token)).getList().size());
        Assert.assertFalse(taskEndpoint.listTask(new TaskListRequest(token)).getList().contains(expectedTask));
    }

    @Category(IntegrationCategory.class)
    @Test
    public void updateByIdTaskTest() {
        @NotNull final Task expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        taskEndpoint.updateByIdTask(new TaskUpdateByIdRequest(token, expectedTask.getId(), "newName", "newDescr"));
        @NotNull final Task actualTask = taskEndpoint.findByIdTask(new TaskFindByIdRequest(token, expectedTask.getId())).getTask();
        Assert.assertEquals("newName", actualTask.getName());
        Assert.assertEquals("newDescr", actualTask.getDescription());
    }

    @Category(IntegrationCategory.class)
    @Test
    public void updateByNameTaskTest() {
        @NotNull final Task expectedTask = taskEndpoint.listTask(new TaskListRequest(token)).getList().get(0);
        taskEndpoint.updateByNameTask(new TaskUpdateByNameRequest(token, expectedTask.getName(), "newName", "newDescr"));
        @NotNull final Task actualTask = taskEndpoint.findByIdTask(new TaskFindByIdRequest(token, expectedTask.getId())).getTask();
        Assert.assertEquals("newName", actualTask.getName());
        Assert.assertEquals("newDescr", actualTask.getDescription());
    }

}
