package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.enumerated.Role;

import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity {

    @Nullable
    private String userId;
    @Nullable
    private Role role;
    @Nullable
    private Date date = new Date();

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Session session = (Session) o;
        return Objects.equals(userId, session.userId) && Objects.equals(role, session.role) && Objects.equals(date, session.date) && getId().equals(session.getId());
    }

}
