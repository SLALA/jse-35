package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.model.Task;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

@Setter
@Getter
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
public final class TaskListByProjectIdResponse extends AbstractResultResponse {

    @XmlElementWrapper
    @NotNull
    private List<Task> list;

    public TaskListByProjectIdResponse(@NotNull List<Task> list) {
        this.list = list;
    }

}
