package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.api.IBusinessService;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.exception.system.IncorrectIndexException;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.*;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    @NotNull
    private final IBusinessRepository<E> entityRepository;

    public AbstractBusinessService(@NotNull final IBusinessRepository<E> entityRepository) {
        super(entityRepository);
        this.entityRepository = entityRepository;
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        return entityRepository.findAll(userId);
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        if (!Optional.ofNullable(comparator).isPresent())
            return new ArrayList<>();
        return entityRepository.findAll(userId, comparator);
    }

    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        entityRepository.clear(userId);
    }

    @NotNull
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(entityRepository.findById(userId, id)).orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public E findByName(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        return Optional.ofNullable(entityRepository.findByName(userId, name)).orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(index).filter((i) -> i >= 0).orElseThrow(IncorrectIndexException::new);
        return Optional.ofNullable(entityRepository.findByIndex(userId, index)).orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public E removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(entityRepository.removeById(userId, id)).orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public E removeByName(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        return Optional.ofNullable(entityRepository.removeByName(userId, name)).orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public E removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(index).filter((i) -> i >= 0).orElseThrow(IncorrectIndexException::new);
        return Optional.ofNullable(entityRepository.removeByIndex(userId, index)).orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public E updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        @NotNull final E entity = findById(userId, id);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @NotNull
    @Override
    public E updateByName(@Nullable final String userId, @Nullable final String oldName, @Nullable final String name, @Nullable final String description) {
        @NotNull final E entity = findByName(userId, oldName);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @NotNull
    @Override
    public E updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        @NotNull final E entity = findByIndex(userId, index);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @NotNull
    @Override
    public E changeStatusById(@Nullable final String userId, @Nullable final String id, @NotNull final Status status) {
        @NotNull final E entity = findById(userId, id);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS)
            entity.setDateStart(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E changeStatusByName(@Nullable final String userId, @Nullable final String name, @NotNull final Status status) {
        @NotNull final E entity = findByName(userId, name);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS)
            entity.setDateStart(new Date());
        return entity;
    }

    @NotNull
    @Override
    public E changeStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @NotNull final Status status) {
        @NotNull final E entity = findByIndex(userId, index);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS)
            entity.setDateStart(new Date());
        return entity;
    }

}
