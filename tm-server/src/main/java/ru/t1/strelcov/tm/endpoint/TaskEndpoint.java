package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;
import ru.t1.strelcov.tm.model.Session;

import javax.jws.WebService;
import java.util.Comparator;
import java.util.Optional;

@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint")
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return serviceLocator.getTaskService();
    }

    @NotNull
    @Override
    public TaskListResponse listTask(@NotNull TaskListRequest request) {
        @NotNull final Session session = check(request);
        return new TaskListResponse(getTaskService().findAll(session.getUserId()));
    }

    @NotNull
    @Override
    public TaskListSortedResponse listSortedTask(@NotNull TaskListSortedRequest request) {
        @NotNull final Session session = check(request);
        final String sort = request.getSort();
        if (SortType.isValidByName(sort)) {
            @NotNull final SortType sortType = SortType.valueOf(sort);
            @NotNull final Comparator comparator = sortType.getComparator();
            return new TaskListSortedResponse(getTaskService().findAll(session.getUserId(), comparator));
        } else
            throw new IncorrectSortOptionException(sort);
    }

    @NotNull
    @Override
    public TaskListByProjectIdResponse listByProjectIdTask(@NotNull TaskListByProjectIdRequest request) {
        @NotNull final Session session = check(request);
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return new TaskListByProjectIdResponse(serviceLocator.getProjectTaskService().findAllTasksByProjectId(session.getUserId(), request.getProjectId()));
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeStatusByIdTask(@NotNull TaskChangeStatusByIdRequest request) {
        @NotNull final Session session = check(request);
        return new TaskChangeStatusByIdResponse(getTaskService().changeStatusById(session.getUserId(), request.getId(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    public TaskChangeStatusByNameResponse changeStatusByNameTask(@NotNull TaskChangeStatusByNameRequest request) {
        @NotNull final Session session = check(request);
        return new TaskChangeStatusByNameResponse(getTaskService().changeStatusByName(session.getUserId(), request.getName(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    public TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        @NotNull final Session session = check(request);
        getTaskService().clear(session.getUserId());
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        @NotNull final Session session = check(request);
        return new TaskCreateResponse(getTaskService().add(session.getUserId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    public TaskFindByIdResponse findByIdTask(@NotNull TaskFindByIdRequest request) {
        @NotNull final Session session = check(request);
        return new TaskFindByIdResponse(getTaskService().findById(session.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public TaskFindByNameResponse findByNameTask(@NotNull TaskFindByNameRequest request) {
        @NotNull final Session session = check(request);
        return new TaskFindByNameResponse(getTaskService().findByName(session.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeByIdTask(@NotNull TaskRemoveByIdRequest request) {
        @NotNull final Session session = check(request);
        return new TaskRemoveByIdResponse(getTaskService().removeById(session.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public TaskRemoveByNameResponse removeByNameTask(@NotNull TaskRemoveByNameRequest request) {
        @NotNull final Session session = check(request);
        return new TaskRemoveByNameResponse(getTaskService().removeByName(session.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) {
        @NotNull final Session session = check(request);
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return new TaskBindToProjectResponse(serviceLocator.getProjectTaskService().bindTaskToProject(session.getUserId(), request.getTaskId(), request.getProjectId()));
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        @NotNull final Session session = check(request);
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return new TaskUnbindFromProjectResponse(serviceLocator.getProjectTaskService().unbindTaskFromProject(session.getUserId(), request.getTaskId()));
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateByIdTask(@NotNull TaskUpdateByIdRequest request) {
        @NotNull final Session session = check(request);
        return new TaskUpdateByIdResponse(getTaskService().updateById(session.getUserId(), request.getId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    public TaskUpdateByNameResponse updateByNameTask(@NotNull TaskUpdateByNameRequest request) {
        @NotNull final Session session = check(request);
        return new TaskUpdateByNameResponse(getTaskService().updateByName(session.getUserId(), request.getOldName(), request.getName(), request.getDescription()));
    }

}
