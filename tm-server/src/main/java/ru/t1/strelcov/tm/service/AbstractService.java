package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.api.IService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.model.AbstractEntity;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void add(@Nullable final E entity) {
        Optional.ofNullable(entity).ifPresent(repository::add);
    }

    @Override
    public void addAll(@Nullable final List<E> list) {
        Optional.ofNullable(list).map(l -> l.stream().filter(Objects::nonNull).collect(Collectors.toList())).ifPresent(repository::addAll);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void remove(@Nullable final E entity) {
        Optional.ofNullable(entity).ifPresent(repository::remove);
    }

    @NotNull
    @Override
    public E findById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(repository.findById(id)).orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public E removeById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(repository.removeById(id)).orElseThrow(EntityNotFoundException::new);
    }

}
