package ru.t1.strelcov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.CheckUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final Task[] TASKS = new Task[]{
            new Task("id2", "pr1"),
            new Task("id1", "pr1"),
            new Task("id2", "pr4"),
            new Task("id3", "pr7"),
            new Task("id1", "pr3"),
            new Task("id3", "pr6")
    };

    @Before
    public void before() {
        repository.addAll(Arrays.asList(TASKS));
    }

    @Test
    public void addTest() {
        @NotNull final Task task = new Task("id1", "pr4");
        int size = repository.findAll().size();
        Assert.assertFalse(repository.findAll().contains(task));
        repository.add(task);
        Assert.assertTrue(repository.findAll().contains(task));
        Assert.assertEquals(size + 1, repository.findAll().size());
    }

    @Test
    public void addAllTest() {
        @NotNull final List<Task> list = Arrays.asList(new Task("id11", "pr1"), new Task("id22", "pr2"));
        int size = repository.findAll().size();
        Assert.assertFalse(repository.findAll().containsAll(list));
        repository.addAll(list);
        Assert.assertTrue(repository.findAll().containsAll(list));
        Assert.assertEquals(size + list.size(), repository.findAll().size());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(Arrays.asList(TASKS).size(), repository.findAll().size());
        Assert.assertTrue(repository.findAll().containsAll(Arrays.asList(TASKS)));
    }

    @Test
    public void clearTest() {
        Assert.assertNotEquals(0, repository.findAll().size());
        repository.clear();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void removeTest() {
        @NotNull final Task task = repository.findAll().get(0);
        int fullSize = repository.findAll().size();
        repository.remove(task);
        Assert.assertFalse(repository.findAll().contains(task));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void findByIdTest() {
        for (@NotNull final Task task : repository.findAll()) {
            Assert.assertTrue(repository.findAll().contains(repository.findById(task.getId())));
            Assert.assertEquals(task, repository.findById(task.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        @NotNull final Task task = repository.findAll().get(0);
        int fullSize = repository.findAll().size();
        Assert.assertNotNull(repository.removeById(task.getId()));
        Assert.assertFalse(repository.findAll().contains(task));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void clearByUserIdTest() {
        @NotNull final Task task = repository.findAll().get(0);
        Assert.assertNotNull(task.getUserId());
        @NotNull final String userId = task.getUserId();
        @NotNull final List<Task> list = repository.findAll(userId);
        int size = list.size();
        int fullSize = repository.findAll().size();
        repository.clear(userId);
        Assert.assertEquals(0, repository.findAll(userId).size());
        Assert.assertEquals(fullSize - size, repository.findAll().size());
    }

    @Test
    public void findAllByUserIdTest() {
        for (@NotNull final Task task : repository.findAll()) {
            if (CheckUtil.isEmpty(task.getUserId())) continue;
            final String userId = task.getUserId();
            @NotNull final List<Task> list = repository.findAll().stream().filter((i) -> userId.equals(i.getUserId())).collect(Collectors.toList());
            Assert.assertTrue(list.containsAll(repository.findAll(userId)));
            Assert.assertEquals(list.size(), repository.findAll(userId).size());
        }
    }

    @Test
    public void findAllSortedByUserIdTest() {
        for (@NotNull final Task task : repository.findAll()) {
            if (CheckUtil.isEmpty(task.getUserId())) continue;
            @NotNull final String userId = task.getUserId();
            for (@NotNull final SortType sortType : SortType.values()) {
                @NotNull final Comparator<Task> comparator = sortType.getComparator();
                @NotNull final List<Task> sortedList = repository.findAll(userId, comparator);
                Assert.assertEquals(sortedList.stream().sorted(comparator)
                        .collect(Collectors.toList()), sortedList);
                @NotNull List<Task> list = repository.findAll().stream().filter((i) -> userId.equals(i.getUserId())).collect(Collectors.toList());
                Assert.assertTrue(list.containsAll(sortedList));
                Assert.assertEquals(list.size(), sortedList.size());
            }
        }
    }

    @Test
    public void findByIdByUserIdTest() {
        for (@NotNull final Task task : repository.findAll()) {
            if (CheckUtil.isEmpty(task.getUserId())) continue;
            Assert.assertEquals(task, repository.findById(task.getUserId(), task.getId()));
        }
    }

    @Test
    public void findByNameByUserIdTest() {
        for (@NotNull final Task task : repository.findAll()) {
            if (CheckUtil.isEmpty(task.getUserId()) || CheckUtil.isEmpty(task.getName())) continue;
            Assert.assertTrue(repository.findAll().contains(repository.findByName(task.getUserId(), task.getName())));
            final Task foundEntity = repository.findByName(task.getUserId(), task.getName());
            Assert.assertNotNull(foundEntity);
            Assert.assertEquals(task.getName(), foundEntity.getName());
        }
    }

    @Test
    public void removeByNameByUserIdTest() {
        @NotNull final Task task = repository.findAll().get(0);
        Assert.assertNotNull(task.getUserId());
        Assert.assertNotNull(task.getName());
        int fullSize = repository.findAll().size();
        Optional.ofNullable(repository.removeByName(task.getUserId(), task.getName())).ifPresent(p -> Assert.assertFalse(repository.findAll().contains(p)));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void removeByIdByUserIdTest() {
        @NotNull final Task task = repository.findAll().get(0);
        Assert.assertNotNull(task.getUserId());
        Assert.assertNotNull(task.getName());
        int fullSize = repository.findAll().size();
        @NotNull final String id = repository.findAll(task.getUserId()).get(0).getId();
        Optional.ofNullable(repository.removeById(task.getUserId(), id)).ifPresent(p -> Assert.assertFalse(repository.findAll().contains(p)));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final String projectId = "pr1";
        @NotNull final Task[] tasks = new Task[]{
                new Task("id2", "t1"),
                new Task("id1", "t1"),
                new Task("id1", "t2")
        };
        for (@NotNull final Task task : tasks) {
            task.setProjectId(projectId);
            repository.add(task);
        }
        for (@NotNull final Task task : repository.findAll()) {
            if (task.getUserId() == null || task.getProjectId() == null) continue;
            @NotNull final List<Task> list = Arrays.stream(tasks)
                    .filter(t -> task.getUserId().equals(t.getUserId()) && task.getProjectId().equals(t.getProjectId()))
                    .collect(Collectors.toList());
            Assert.assertTrue(list.containsAll(repository.findAllByProjectId(task.getUserId(), projectId)));
            Assert.assertEquals(list.size(), repository.findAllByProjectId(task.getUserId(), projectId).size());
        }
    }

    @Test
    public void removeAllByProjectId() {
        @NotNull final String projectId = "pr1";
        @NotNull final String userId = "id1";
        @NotNull final Task[] tasks = new Task[]{
                new Task(userId, "t1"),
                new Task(userId, "t3"),
                new Task(userId, "t2")
        };
        for (@NotNull final Task task : tasks) {
            task.setProjectId(projectId);
            repository.add(task);
        }
        int fullSize = repository.findAll().size();
        repository.removeAllByProjectId(userId, projectId);
        Assert.assertEquals(0, repository.findAllByProjectId(userId, projectId).size());
        Assert.assertEquals(fullSize - tasks.length, repository.findAll().size());
    }

}
