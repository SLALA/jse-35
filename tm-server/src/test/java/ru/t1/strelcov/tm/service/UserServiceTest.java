package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.AbstractException;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.repository.UserRepository;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Vector;

public class UserServiceTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService service = new UserService(repository, propertyService);

    @NotNull
    private static final String LOGIN_UNIQUE = "Login_Unique";

    @NotNull
    private static final String PASS = "PASS";

    @NotNull
    private final String PASS_HASH = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), PASS);

    @NotNull
    private final User[] USERS = new User[]{
            new User("admin", PASS_HASH, Role.ADMIN),
            new User("test1", PASS_HASH, Role.USER),
            new User("admin0", PASS_HASH, Role.ADMIN),
            new User("test2", PASS_HASH, Role.USER),
            new User("admin1", PASS_HASH, Role.ADMIN),
            new User("test", PASS_HASH, Role.USER)
    };

    @Before
    public void before() {
        repository.addAll(Arrays.asList(USERS));
    }

    @Test
    public void addTest() {
        @NotNull final User user = new User("id1", "pr1");
        int size = service.findAll().size();
        service.add(null);
        Assert.assertEquals(size, service.findAll().size());
        service.add(user);
        Assert.assertEquals(size + 1, service.findAll().size());
        Assert.assertTrue(service.findAll().contains(user));
    }

    @Test
    public void addAllTest() {
        @NotNull final User[] newUsers = new User[]{new User("id11", "pr11"), new User("id22", "pr22")};
        @NotNull final Vector<User> list = new Vector<>(Arrays.asList(newUsers));
        int size = service.findAll().size();
        int listSize = list.size();
        @NotNull final Vector<User> listWithNulls = new Vector<>(list);
        listWithNulls.addAll(Arrays.asList(null, null));
        service.addAll(listWithNulls);
        Assert.assertEquals(size + listSize, service.findAll().size());
        Assert.assertTrue(service.findAll().containsAll(list));
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(Arrays.asList(USERS).size(), service.findAll().size());
        Assert.assertTrue(service.findAll().containsAll(Arrays.asList(USERS)));
    }

    @Test
    public void clearTest() {
        Assert.assertNotEquals(0, service.findAll().size());
        service.clear();
        Assert.assertEquals(0, service.findAll().size());
    }

    @Test
    public void removeTest() {
        @NotNull final User user = service.findAll().get(0);
        int fullSize = service.findAll().size();
        service.remove(user);
        Assert.assertFalse(service.findAll().contains(user));
        Assert.assertEquals(fullSize - 1, service.findAll().size());
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.findById(null));
        Assert.assertThrows(AbstractException.class, () -> service.findById("notExistedId"));
        for (@NotNull final User user : service.findAll()) {
            Assert.assertTrue(service.findAll().contains(service.findById(user.getId())));
            Assert.assertEquals(user, service.findById(user.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        Assert.assertThrows(AbstractException.class, () -> service.removeById(null));
        Assert.assertThrows(AbstractException.class, () -> service.removeById("notExistedId"));
        @NotNull final User user = service.findAll().get(0);
        int fullSize = service.findAll().size();
        Assert.assertNotNull(service.removeById(user.getId()));
        Assert.assertFalse(service.findAll().contains(user));
        Assert.assertEquals(fullSize - 1, service.findAll().size());
    }

    @Test
    public void addWithRole() {
        Assert.assertThrows(AbstractException.class, () -> service.add(null, "notExistedId", Role.ADMIN));
        Assert.assertThrows(AbstractException.class, () -> service.add("notExistedId", null, Role.ADMIN));
        int size = service.findAll().size();
        @Nullable User user = service.add(LOGIN_UNIQUE, PASS, Role.ADMIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(size + 1, service.findAll().size());
        Assert.assertTrue(service.findAll().contains(user));
        Assert.assertEquals(LOGIN_UNIQUE, user.getLogin());
        Assert.assertEquals(Role.ADMIN, user.getRole());
        @NotNull String expectedPassHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), PASS);
        Assert.assertEquals(expectedPassHash, user.getPasswordHash());
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(AbstractException.class, () -> service.findByLogin("notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.findByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> service.findByLogin(""));
        service.add(LOGIN_UNIQUE, PASS, Role.ADMIN);
        @Nullable User user = service.findByLogin(LOGIN_UNIQUE);
        Assert.assertNotNull(user);
        Assert.assertEquals(LOGIN_UNIQUE, user.getLogin());
    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(AbstractException.class, () -> service.removeByLogin("notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.removeByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> service.removeByLogin(""));
        int fullSize = service.findAll().size();
        @Nullable User user = service.removeByLogin(service.findAll().get(0).getLogin());
        Assert.assertNotNull(user);
        Assert.assertFalse(service.findAll().contains(user));
        Assert.assertEquals(fullSize - 1, service.findAll().size());
    }

    @Test
    public void updateByLogin() {
        Assert.assertThrows(AbstractException.class, () -> service.updateByLogin("notExistedId", "name", "fam", "", "email"));
        Assert.assertThrows(AbstractException.class, () -> service.updateByLogin(null, "name", "fam", "", "email"));
        Assert.assertThrows(AbstractException.class, () -> service.updateByLogin("", "name", "fam", "", "email"));
        @Nullable User user = service.updateByLogin(service.findAll().get(0).getLogin(), "name", "fam", "otch", "email");
        Assert.assertNotNull(user);
        Assert.assertEquals("name", user.getFirstName());
        Assert.assertEquals("fam", user.getLastName());
        Assert.assertEquals("email", user.getEmail());
        Assert.assertEquals("otch", user.getMiddleName());
    }

    @Test
    public void changePasswordById() {
        Assert.assertThrows(AbstractException.class, () -> service.changePasswordById("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> service.changePasswordById(null, "name"));
        @Nullable final User user = service.findAll().get(0);
        Assert.assertNotNull(user);
        @NotNull final String id = user.getId();
        @NotNull final String newPass = PASS + "new";
        @NotNull String expectedPassHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), newPass);
        service.changePasswordById(id, newPass);
        Assert.assertEquals(expectedPassHash, user.getPasswordHash());
    }

    @Test
    public void lockUnlockByLogin() {
        Assert.assertThrows(AbstractException.class, () -> service.lockUserByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> service.lockUserByLogin("notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> service.unlockUserByLogin(null));
        Assert.assertThrows(AbstractException.class, () -> service.unlockUserByLogin("notExistedId"));
        for (@NotNull final User user : service.findAll()) {
            @NotNull final String login = user.getLogin();
            Assert.assertNotNull(user.getRole());
            @NotNull final Role role = user.getRole();
            if (role == Role.ADMIN)
                Assert.assertThrows(AbstractException.class, () -> service.lockUserByLogin(login));
            else {
                service.lockUserByLogin(login);
                Assert.assertEquals(true, user.getLocked());
                service.unlockUserByLogin(login);
                Assert.assertEquals(false, user.getLocked());
            }
        }
    }

}
