package ru.t1.strelcov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.CheckUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final Project[] PROJECTS = new Project[]{
            new Project("id2", "pr1"),
            new Project("id1", "pr1"),
            new Project("id2", "pr4"),
            new Project("id3", "pr7"),
            new Project("id1", "pr3"),
            new Project("id3", "pr6")
    };

    @Before
    public void before() {
        repository.addAll(Arrays.asList(PROJECTS));
    }

    @Test
    public void addTest() {
        @NotNull final Project project = new Project("id1", "pr4");
        int size = repository.findAll().size();
        Assert.assertFalse(repository.findAll().contains(project));
        repository.add(project);
        Assert.assertTrue(repository.findAll().contains(project));
        Assert.assertEquals(size + 1, repository.findAll().size());
    }

    @Test
    public void addAllTest() {
        @NotNull final List<Project> list = Arrays.asList(new Project("id11", "pr1"), new Project("id22", "pr2"));
        int size = repository.findAll().size();
        Assert.assertFalse(repository.findAll().containsAll(list));
        repository.addAll(list);
        Assert.assertTrue(repository.findAll().containsAll(list));
        Assert.assertEquals(size + list.size(), repository.findAll().size());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(Arrays.asList(PROJECTS).size(), repository.findAll().size());
        Assert.assertTrue(repository.findAll().containsAll(Arrays.asList(PROJECTS)));
    }

    @Test
    public void clearTest() {
        Assert.assertNotEquals(0, repository.findAll().size());
        repository.clear();
        Assert.assertEquals(0, repository.findAll().size());
    }

    @Test
    public void removeTest() {
        @NotNull final Project project = repository.findAll().get(0);
        int fullSize = repository.findAll().size();
        repository.remove(project);
        Assert.assertFalse(repository.findAll().contains(project));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void findByIdTest() {
        for (@NotNull final Project project : repository.findAll()) {
            Assert.assertTrue(repository.findAll().contains(repository.findById(project.getId())));
            Assert.assertEquals(project, repository.findById(project.getId()));
        }
    }

    @Test
    public void removeByIdTest() {
        @NotNull final Project project = repository.findAll().get(0);
        int fullSize = repository.findAll().size();
        Assert.assertNotNull(repository.removeById(project.getId()));
        Assert.assertFalse(repository.findAll().contains(project));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void clearByUserIdTest() {
        @NotNull Project project = repository.findAll().get(0);
        Assert.assertNotNull(project.getUserId());
        @NotNull final String userId = project.getUserId();
        @NotNull final List<Project> list = repository.findAll(userId);
        int size = list.size();
        int fullSize = repository.findAll().size();
        repository.clear(userId);
        Assert.assertEquals(0, repository.findAll(userId).size());
        Assert.assertEquals(fullSize - size, repository.findAll().size());
    }

    @Test
    public void findAllByUserIdTest() {
        for (@NotNull final Project project : repository.findAll()) {
            if (CheckUtil.isEmpty(project.getUserId())) continue;
            final String userId = project.getUserId();
            @NotNull final List<Project> list = repository.findAll().stream().filter((i) -> userId.equals(i.getUserId())).collect(Collectors.toList());
            Assert.assertTrue(list.containsAll(repository.findAll(userId)));
            Assert.assertEquals(list.size(), repository.findAll(userId).size());
        }
    }

    @Test
    public void findAllSortedByUserIdTest() {
        for (@NotNull final Project project : repository.findAll()) {
            if (CheckUtil.isEmpty(project.getUserId())) continue;
            @NotNull final String userId = project.getUserId();
            for (@NotNull final SortType sortType : SortType.values()) {
                @NotNull final Comparator<Project> comparator = sortType.getComparator();
                @NotNull final List<Project> sortedList = repository.findAll(userId, comparator);
                Assert.assertEquals(sortedList.stream().sorted(comparator)
                        .collect(Collectors.toList()), sortedList);
                @NotNull List<Project> list = repository.findAll().stream().filter((i) -> userId.equals(i.getUserId())).collect(Collectors.toList());
                Assert.assertTrue(list.containsAll(sortedList));
                Assert.assertEquals(list.size(), sortedList.size());
            }
        }
    }

    @Test
    public void findByIdByUserIdTest() {
        for (@NotNull final Project project : repository.findAll()) {
            if (CheckUtil.isEmpty(project.getUserId())) continue;
            Assert.assertEquals(project, repository.findById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void findByNameByUserIdTest() {
        for (@NotNull final Project project : repository.findAll()) {
            if (CheckUtil.isEmpty(project.getUserId()) || CheckUtil.isEmpty(project.getName())) continue;
            Assert.assertTrue(repository.findAll().contains(repository.findByName(project.getUserId(), project.getName())));
            final Project foundEntity = repository.findByName(project.getUserId(), project.getName());
            Assert.assertNotNull(foundEntity);
            Assert.assertEquals(project.getName(), foundEntity.getName());
        }
    }

    @Test
    public void removeByNameByUserIdTest() {
        @NotNull final Project project = repository.findAll().get(0);
        Assert.assertNotNull(project.getUserId());
        Assert.assertNotNull(project.getName());
        int fullSize = repository.findAll().size();
        Optional.ofNullable(repository.removeByName(project.getUserId(), project.getName())).ifPresent(p -> Assert.assertFalse(repository.findAll().contains(p)));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

    @Test
    public void removeByIdByUserIdTest() {
        @NotNull final Project project = repository.findAll().get(0);
        Assert.assertNotNull(project.getUserId());
        Assert.assertNotNull(project.getName());
        int fullSize = repository.findAll().size();
        @NotNull final String id = repository.findAll(project.getUserId()).get(0).getId();
        Optional.ofNullable(repository.removeById(project.getUserId(), id)).ifPresent(p -> Assert.assertFalse(repository.findAll().contains(p)));
        Assert.assertEquals(fullSize - 1, repository.findAll().size());
    }

}
